FROM docker.io/python:3.7-alpine AS base

# Registry Exporter image for OpenShift Origin

COPY requirements.txt /requirements.txt

RUN mkdir /install \
    && cd /install \
    && apk add --no-cache --virtual=build-dependencies \
	autoconf automake g++ gcc linux-headers make openssl-dev zlib-dev \
    && pip install --prefix=/install -r /requirements.txt

FROM docker.io/python:3.7-alpine

LABEL io.k8s.description="Registry Prometheus Exporter Image." \
      io.k8s.display-name="Registry Prometheus Exporter" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="prometheus,exporter,docker,registry" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-registryexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.1"

COPY --from=base /install /usr/local
COPY exporter /exporter

WORKDIR /exporter

ENTRYPOINT ["python", "/exporter/exporter.py"]
USER 1001
